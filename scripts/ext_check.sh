#!/bin/bash

APP_CHECK_URL="http://${HAPROXY_SERVER_ADDR}:9099/api/test/state"

NC_TIMEOUT=1
CURL_TIMEOUT=1
CURL_MAX_TIME=1

if [[ "$(curl -s --connect-timeout ${CURL_TIMEOUT} --max-time ${CURL_MAX_TIME} --write-out %{http_code} --output /dev/null ${APP_CHECK_URL} 2>/dev/null)" == 200 ]]; then
    echo "`date` check ${APP_CHECK_URL} is passed " >> /tmp/check.log
    exit 0
fi

echo "`date` check ${APP_CHECK_URL} is fail " >> /tmp/check.log
exit 1